public class Driver {
    public static void main(String args[]){

        //SingletonClass obj = new SingletonClass();       //<- cant directly  create object
        System.out.println(EagerInitializationSingleton.getInstance());
        System.out.println(EagerInitializationSingleton.getInstance());
    }
}
