/**
 * Easiest way to implement Singleton.
 * But,it has a drawback that instance is created even though client application might not be using it.
 * Singleton classes are created for resources such as File System, Database connections etc.
 * So, We should avoid the instantiation unless needed.
 * Also, this method doesn’t provide any options for exception handling. Cure static block initialization
 */

public class EagerInitializationSingleton {
    /**
     * Private static variable of the same class that is the only instance of the class
     *
     */
    private static EagerInitializationSingleton instance = new EagerInitializationSingleton();
    /**
     *  Private constructor to restrict instantiation of the class from other class
     *  to avoid client applications to use constructor
     */
    private EagerInitializationSingleton(){

    }

    /**
     * Public static method that returns the instance of the class, this is the global access pt.
     * @return
     */
    public static EagerInitializationSingleton getInstance(){
        return instance;
    }
}
