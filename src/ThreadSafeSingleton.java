/**
 *
 */
public class ThreadSafeSingleton  {
    private static ThreadSafeSingleton instance;
    private ThreadSafeSingleton(){

    }

    /**
     * Below impl reduces the performance because of the cost associated with the synchronized method,
     * although we need it only for the first few threads who might create the separate instances
     * Cure: double checked locking principle
     * @return
     */
    public static synchronized ThreadSafeSingleton getInstance() {
        if(instance == null){
            instance = new ThreadSafeSingleton();
        }
        return instance;
    }

    /**
     * double checked locking principle is used.
     * In this approach, the synchronized block is used inside the if condition
     * with an additional check to ensure that only one instance of a singleton class is created.
     * @return
     */
    public static ThreadSafeSingleton getInstanceUsingDoubleLocking() {
        if(instance == null){
            synchronized (ThreadSafeSingleton.class) {
                if (instance == null) {
                    instance = new ThreadSafeSingleton();
                }
            }
        }
        return instance;
    }
}
